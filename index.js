const express = require("express")
const mongoose = require("mongoose")
const dotenv = require("dotenv")

dotenv.config()

const app = express()

const port = 4002

mongoose.connect(`mongodb+srv://bks0381:${process.env.MONGODB_PASSWORD}@cluster0.0atlgec.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on("error", () => console.error("connection error"))
db.on("open", () => console.log("connected to MongoDB"))

app.use(express.json())
app.use(express.urlencoded({extended: true}))

const userSchema = new mongoose.Schema({
	name: String,
	password: String
})

const User = mongoose.model("User", userSchema)

/*
3. Create a POST route that will access the /signup route that will create
a user.
*/

/*
4. Process a POST request at the /signup route using postman to
register a user.
*/
app.post("/signup", (request, response) => {
	if (request.body.name == null || request.body.password == null) {

		response.send(`Please input username and password.`)
	}

	let newUser = new User({
			name: request.body.name,
			password: request.body.password
		})

		newUser.save((error, savedUser) => {
			if (error) {
				return console.error(error)
			} else {
				return response.status(200).send(`You're now registered.`)
			}
		})
})

app.listen(port, () => console.log(`Server running at localhost: ${port}`))